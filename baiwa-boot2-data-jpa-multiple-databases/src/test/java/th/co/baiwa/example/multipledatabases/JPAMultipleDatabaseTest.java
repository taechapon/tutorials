package th.co.baiwa.example.multipledatabases;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import th.co.baiwa.buckwaframework.common.config.AppConfigTest;
import th.co.baiwa.buckwaframework.common.constant.CommonConstants.PROFILE;
import th.co.baiwa.buckwaframework.preferences.persistence.repository.BuckwaDemoRepository;
import th.co.baiwa.example.multipledatabases.mysql.persistence.repository.MessageRepository;
import th.co.baiwa.example.multipledatabases.oracle.persistence.repository.RoleRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfigTest.class)
@ActiveProfiles(PROFILE.UNITTEST)
public class JPAMultipleDatabaseTest {
	
	@Autowired
	private BuckwaDemoRepository buckwaDemoRepository;
	
	@Autowired
	private MessageRepository messageRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Test
	public void test() {
		System.out.println("wow");
		
		System.out.println("From Local MySQL");
		buckwaDemoRepository.findAll().forEach(
			buckwaDemo -> System.out.println(buckwaDemo.getDemoCode())
		);
		
		System.out.println("From BaiwaServer MySql");
		messageRepository.findAll().forEach(
			message -> System.out.println(message.getMessageCode())
		);
		
		System.out.println("From BaiwaServer Oracle");
		roleRepository.findAll().forEach(
			role -> System.out.println(role.getRoleCode())
		);
	}
	
}
