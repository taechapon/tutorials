package th.co.baiwa.example.multipledatabases.oracle.persistence.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import th.co.baiwa.buckwaframework.common.persistence.jdbc.CommonJdbcTemplate;
import th.co.baiwa.example.multipledatabases.oracle.persistence.entity.Role;

@Repository
public class RoleRepository {
	
	@Autowired
	@Qualifier("oracleCommonJdbcTemplate")
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<Role> findAll() {
		String sql = "SELECT * FROM EXCISEADM.ADM_ROLE";
		
		List<Role> resultList = commonJdbcTemplate.executeQuery(sql, new RowMapper<Role>() {
			@Override
			public Role mapRow(ResultSet rs, int rowNum) throws SQLException {
				Role role = new Role();
				role.setRoleCode(rs.getString("ROLE_CODE"));
				return role;
			}
		});
		
		return resultList;
	}
	
}
