package th.co.baiwa.example.multipledatabases.mysql.persistence.repository;

import th.co.baiwa.buckwaframework.common.persistence.repository.CommonJpaCrudRepository;
import th.co.baiwa.example.multipledatabases.mysql.persistence.entity.Message;

public interface MessageRepository extends CommonJpaCrudRepository<Message, Long> {

}
