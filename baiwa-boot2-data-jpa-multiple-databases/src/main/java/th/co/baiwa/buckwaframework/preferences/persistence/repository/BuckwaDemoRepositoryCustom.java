package th.co.baiwa.buckwaframework.preferences.persistence.repository;

import java.util.List;

import th.co.baiwa.buckwaframework.preferences.persistence.entity.BuckwaDemo;

public interface BuckwaDemoRepositoryCustom {
	
	public List<BuckwaDemo> findByCriteria(BuckwaDemo criteria);

	public int[][] batchInsert(List<BuckwaDemo> buckwaDemoList, int batchSize);
	
}
