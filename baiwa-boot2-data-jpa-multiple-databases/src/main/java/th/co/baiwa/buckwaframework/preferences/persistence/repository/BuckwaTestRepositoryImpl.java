package th.co.baiwa.buckwaframework.preferences.persistence.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import th.co.baiwa.buckwaframework.common.persistence.jdbc.CommonJdbcTemplate;
import th.co.baiwa.buckwaframework.preferences.persistence.entity.BuckwaTest;

public class BuckwaTestRepositoryImpl implements BuckwaTestRepositoryCustom {
	
	@Autowired
    private CommonJdbcTemplate commonJdbcTemplate;
    
    @Override
    public BuckwaTest findByPk(Long id) {
    	String sql = "SELECT * FROM buckwa_test WHERE col_pk = ?";
        return commonJdbcTemplate.executeQueryForObject(sql,
        	new Object[] {
        		id
        	},
        	new RowMapper<BuckwaTest>() {
				@Override
				public BuckwaTest mapRow(ResultSet rs, int rowNum) throws SQLException {
					BuckwaTest buckwaTest = new BuckwaTest();
					buckwaTest.setColPk(rs.getLong("col_pk"));
					buckwaTest.setColVarchar(rs.getString("col_varchar"));
					buckwaTest.setColInt(rs.getInt("col_int"));
					buckwaTest.setColDouble(rs.getDouble("col_double"));
					buckwaTest.setColDecimal(rs.getBigDecimal("col_decimal"));
					buckwaTest.setColTimestamp(rs.getTimestamp("col_timestamp").toLocalDateTime());
					buckwaTest.setColDate(rs.getDate("col_date").toLocalDate());
					buckwaTest.setColTime(rs.getTime("col_time").toLocalTime());
					return buckwaTest;
				}
        	}
        );
    }
	
}
