package th.co.baiwa.buckwaframework.preferences.persistence.repository;

import th.co.baiwa.buckwaframework.preferences.persistence.entity.BuckwaTest;

public interface BuckwaTestRepositoryCustom {
	
	public BuckwaTest findByPk(Long id);
	
}
