package th.co.baiwa.buckwaframework.common.config;

import java.sql.SQLException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.dialect.MySQL5Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

import th.co.baiwa.buckwaframework.common.persistence.jdbc.CommonJdbcTemplate;
import th.co.baiwa.buckwaframework.common.persistence.repository.support.CommonSimpleJpaRepository;

@Configuration
@EnableJpaRepositories(
	basePackages = {
		"th.co.baiwa.buckwaframework",
	},
	repositoryBaseClass = CommonSimpleJpaRepository.class,
	entityManagerFactoryRef = "entityManagerFactory",
	transactionManagerRef = "transactionManager"
)
@EnableTransactionManagement
public class DataSourceConfig {
	
	@Autowired
	private Environment env;
	
	@Bean(name = "dataSource")
	@Primary
	public DataSource dataSource() throws SQLException {
		HikariDataSource dataSource = new HikariDataSource();
		dataSource.setDriverClassName(env.getProperty("app.datasource.local.driver-class-name"));
		dataSource.setJdbcUrl(env.getProperty("app.datasource.local.url"));
		dataSource.setUsername(env.getProperty("app.datasource.local.username"));
		dataSource.setPassword(env.getProperty("app.datasource.local.password"));
		return dataSource;
	}
	
	@Bean(name = "commonJdbcTemplate")
	@Primary
	public CommonJdbcTemplate commonJdbcTemplate(@Qualifier("dataSource") DataSource dataSource) {
		return new CommonJdbcTemplate(dataSource);
	}
	
	@Bean(name = "entityManagerFactory")
	@Primary
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(@Qualifier("dataSource") DataSource dataSource) {
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setJpaProperties(additionalProperties());
		factory.setPackagesToScan(
			"th.co.baiwa.buckwaframework"
		);
		factory.setDataSource(dataSource);
		
		return factory;
	}
	
	private Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", MySQL5Dialect.class.getName());
		
		return properties;
	}
	
	@Bean(name = "transactionManager")
	@Primary
	public PlatformTransactionManager transactionManager(@Qualifier("entityManagerFactory") EntityManagerFactory emf) {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(emf);
		return txManager;
	}
	
}
