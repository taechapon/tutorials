package th.co.baiwa.buckwaframework.preferences.persistence.repository;

import org.springframework.data.jpa.repository.Query;

import th.co.baiwa.buckwaframework.common.constant.CommonConstants.FLAG;
import th.co.baiwa.buckwaframework.common.persistence.repository.CommonJpaPagingAndSortingRepository;
import th.co.baiwa.buckwaframework.preferences.persistence.entity.BuckwaDemo;

public interface BuckwaDemoRepository extends CommonJpaPagingAndSortingRepository<BuckwaDemo, Long>, BuckwaDemoRepositoryCustom {
	
	// Query Method
	@Query("select e from BuckwaDemo e where e.isDeleted = '" + FLAG.N_FLAG + "' and demoCode = ?1")
	public BuckwaDemo findByDemoCode(String demoCode);
	
	// Query Method with Native Query
	@Query(
		value = "SELECT * FROM BUCKWA_DEMO WHERE IS_DELETED = '" + FLAG.N_FLAG + "' AND DEMO_DESC = ?1",
		nativeQuery = true
	)
	public BuckwaDemo findByDemoDesc(String demoDesc);
	
	public BuckwaDemo findByDemoCodeAndDemoDesc(String demoCode, String demoDesc);
	
}
