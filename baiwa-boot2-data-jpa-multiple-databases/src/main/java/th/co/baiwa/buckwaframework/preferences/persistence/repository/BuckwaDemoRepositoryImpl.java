package th.co.baiwa.buckwaframework.preferences.persistence.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import th.co.baiwa.buckwaframework.common.persistence.jdbc.CommonJdbcTemplate;
import th.co.baiwa.buckwaframework.preferences.persistence.entity.BuckwaDemo;

public class BuckwaDemoRepositoryImpl implements BuckwaDemoRepositoryCustom {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	@Override
	public List<BuckwaDemo> findByCriteria(BuckwaDemo criteria) {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<>();
		
		sql.append(" SELECT demo_id, demo_code, demo_desc ");
		sql.append(" FROM buckwa_demo ");
		sql.append(" WHERE is_deleted = 'N' ");
		
		if (StringUtils.isNotBlank(criteria.getDemoCode())) {
			sql.append(" AND demo_code = ? ");
			params.add(criteria.getDemoCode());
		}
		if (StringUtils.isNotBlank(criteria.getDemoDesc())) {
			sql.append(" AND demo_desc LIKE ? ");
			params.add("%" + criteria.getDemoDesc() + "%");
		}
		
		List<BuckwaDemo> resultList = commonJdbcTemplate.executeQuery(sql.toString(),
			params.toArray(),
			new RowMapper<BuckwaDemo>() {
				@Override
				public BuckwaDemo mapRow(ResultSet rs, int rowNum) throws SQLException {
					BuckwaDemo buckwaDemo = new BuckwaDemo();
					buckwaDemo.setDemoId(rs.getLong("DEMO_ID"));
					buckwaDemo.setDemoCode(rs.getString("DEMO_CODE"));
					buckwaDemo.setDemoDesc(rs.getString("DEMO_DESC"));
					return buckwaDemo;
				}
			}
		);
		
		return resultList;
	}

	@Override
	public int[][] batchInsert(List<BuckwaDemo> buckwaDemoList, int batchSize) {
		
		int[][] results = null;
		
		StringBuilder sql = new StringBuilder();
		sql.append(" INSERT INTO buckwa_demo (demo_code, demo_desc, is_deleted, version, created_by, created_date) ");
		sql.append(" VALUES (?,?,'N', 1,'BATCH',NOW()) ");
		
		try {
			results = commonJdbcTemplate.executeBatch(sql.toString(), buckwaDemoList, batchSize,
				new ParameterizedPreparedStatementSetter<BuckwaDemo>() {
					@Override
					public void setValues(PreparedStatement ps, BuckwaDemo buckwaDemo) throws SQLException {
						commonJdbcTemplate.preparePs(ps, new Object[] {
							buckwaDemo.getDemoCode(),
							buckwaDemo.getDemoDesc()
						});
					}
				}
			);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return results;
	}
	
}
