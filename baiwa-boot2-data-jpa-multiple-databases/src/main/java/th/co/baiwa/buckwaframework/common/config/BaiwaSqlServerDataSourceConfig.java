package th.co.baiwa.buckwaframework.common.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import th.co.baiwa.buckwaframework.common.persistence.jdbc.CommonJdbcTemplate;

@Configuration
@EnableTransactionManagement
public class BaiwaSqlServerDataSourceConfig {
	
	@Autowired
	private Environment env;
	
	@Bean(name = "sqlServerDataSource")
	public DataSource dataSource() throws SQLException {
		SQLServerDataSource dataSource = new SQLServerDataSource();
		dataSource.setUser(env.getProperty("app.datasource.sqlserver.username"));
		dataSource.setPassword(env.getProperty("app.datasource.sqlserver.password"));
		dataSource.setServerName(env.getProperty("app.datasource.sqlserver.server-name"));
		dataSource.setPortNumber(Integer.parseInt(env.getProperty("app.datasource.sqlserver.port-number")));
		dataSource.setDatabaseName(env.getProperty("app.datasource.sqlserver.database-name"));
		return dataSource;
	}
	
	@Bean(name = "sqlServerCommonJdbcTemplate")
	public CommonJdbcTemplate commonJdbcTemplate(@Qualifier("sqlServerDataSource") DataSource dataSource) {
		return new CommonJdbcTemplate(dataSource);
	}
	
	@Bean(name = "sqlServerTransactionManager")
	public PlatformTransactionManager annotationDrivenTransactionManager(@Qualifier("sqlServerDataSource") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}
	
}
