package th.co.baiwa.buckwaframework.common.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

import th.co.baiwa.buckwaframework.common.persistence.jdbc.CommonJdbcTemplate;

@Configuration
@EnableTransactionManagement
public class BaiwaOracleDataSourceConfig {
	
	@Autowired
	private Environment env;
	
	@Bean(name = "oracleDataSource")
	public DataSource dataSource() throws SQLException {
		HikariDataSource dataSource = new HikariDataSource();
		dataSource.setDriverClassName(env.getProperty("app.datasource.oracle.driver-class-name"));
		dataSource.setJdbcUrl(env.getProperty("app.datasource.oracle.url"));
		dataSource.setUsername(env.getProperty("app.datasource.oracle.username"));
		dataSource.setPassword(env.getProperty("app.datasource.oracle.password"));
		return dataSource;
	}
	
	@Bean(name = "oracleCommonJdbcTemplate")
	public CommonJdbcTemplate commonJdbcTemplate(@Qualifier("oracleDataSource") DataSource dataSource) {
		return new CommonJdbcTemplate(dataSource);
	}
	
	@Bean(name = "oracleTransactionManager")
	public PlatformTransactionManager annotationDrivenTransactionManager(@Qualifier("oracleDataSource") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}
	
}
