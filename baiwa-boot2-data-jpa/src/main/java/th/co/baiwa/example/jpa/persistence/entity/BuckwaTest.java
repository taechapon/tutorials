package th.co.baiwa.example.jpa.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name = "BUCKWA_TEST")
public class BuckwaTest implements Serializable {
	
	private static final long serialVersionUID = -6371693849235439690L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COL_PK")
	private Long colPk;
	
	@Column(name = "COL_VARCHAR")
	private String colVarchar;
	
	@Column(name = "COL_INT")
	private Integer colInt;
	
	@Column(name = "COL_DOUBLE")
	private Double colDouble;
	
	@Column(name = "COL_DECIMAL")
	private BigDecimal colDecimal;
	
	@Column(name = "COL_TIMESTAMP")
	private LocalDateTime colTimestamp;
	
	@Column(name = "COL_DATE")
	private LocalDate colDate;
	
	@Column(name = "COL_TIME")
	private LocalTime colTime;

	public Long getColPk() {
		return colPk;
	}

	public void setColPk(Long colPk) {
		this.colPk = colPk;
	}

	public String getColVarchar() {
		return colVarchar;
	}

	public void setColVarchar(String colVarchar) {
		this.colVarchar = colVarchar;
	}

	public Integer getColInt() {
		return colInt;
	}

	public void setColInt(Integer colInt) {
		this.colInt = colInt;
	}

	public Double getColDouble() {
		return colDouble;
	}

	public void setColDouble(Double colDouble) {
		this.colDouble = colDouble;
	}

	public BigDecimal getColDecimal() {
		return colDecimal;
	}

	public void setColDecimal(BigDecimal colDecimal) {
		this.colDecimal = colDecimal;
	}

	public LocalDateTime getColTimestamp() {
		return colTimestamp;
	}

	public void setColTimestamp(LocalDateTime colTimestamp) {
		this.colTimestamp = colTimestamp;
	}

	public LocalDate getColDate() {
		return colDate;
	}

	public void setColDate(LocalDate colDate) {
		this.colDate = colDate;
	}

	public LocalTime getColTime() {
		return colTime;
	}

	public void setColTime(LocalTime colTime) {
		this.colTime = colTime;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

}
