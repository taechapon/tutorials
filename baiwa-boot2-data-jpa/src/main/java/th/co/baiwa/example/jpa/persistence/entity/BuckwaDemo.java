package th.co.baiwa.example.jpa.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import th.co.baiwa.buckwaframework.common.persistence.entity.BaseEntity;

@Entity
@Table(name = "BUCKWA_DEMO")
public class BuckwaDemo extends BaseEntity {

	private static final long serialVersionUID = -1826276491134899472L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "demo_id")
	private Long demoId;

	@Column(name = "demo_code")
	private String demoCode;

	@Column(name = "demo_desc")
	private String demoDesc;

	public Long getDemoId() {
		return demoId;
	}

	public void setDemoId(Long demoId) {
		this.demoId = demoId;
	}

	public String getDemoCode() {
		return demoCode;
	}

	public void setDemoCode(String demoCode) {
		this.demoCode = demoCode;
	}

	public String getDemoDesc() {
		return demoDesc;
	}

	public void setDemoDesc(String demoDesc) {
		this.demoDesc = demoDesc;
	}

}
