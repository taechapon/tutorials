package th.co.baiwa.example.jpa.service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.co.baiwa.example.jpa.persistence.entity.BuckwaDemo;
import th.co.baiwa.example.jpa.persistence.repository.BuckwaDemoRepository;

@Service
public class BuckwaDemoService {
	
	@Autowired
	private BuckwaDemoRepository buckwaDemoRepository;
	
	@Transactional(rollbackFor = SQLException.class)
	public void execute(BuckwaDemo demo1, BuckwaDemo demo2) throws SQLException {
		buckwaDemoRepository.save(demo1);
		if (demo2 == null) {
			throw new SQLException("Bomb!!");
		}
		buckwaDemoRepository.save(demo2);
	}
	
}
