package th.co.baiwa.example.jpa.persistence.repository;

import th.co.baiwa.example.jpa.persistence.entity.BuckwaTest;

public interface BuckwaTestRepositoryCustom {
	
	public BuckwaTest findByPk(Long id);
	
}
