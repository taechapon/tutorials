package th.co.baiwa.example.jpa.persistence.repository;

import org.springframework.data.repository.CrudRepository;

import th.co.baiwa.example.jpa.persistence.entity.BuckwaTest;

public interface BuckwaTestRepository extends CrudRepository<BuckwaTest, Long>, BuckwaTestRepositoryCustom {
	
	BuckwaTest findByColVarchar(String colVarchar);
	
	BuckwaTest findByColVarcharAndColInt(String colVarchar, int colInt);
	
}
