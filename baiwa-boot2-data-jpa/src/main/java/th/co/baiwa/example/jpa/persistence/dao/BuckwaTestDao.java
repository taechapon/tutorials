package th.co.baiwa.example.jpa.persistence.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import th.co.baiwa.example.jpa.persistence.entity.BuckwaTest;

@Repository
@Transactional
public class BuckwaTestDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public BuckwaTest findById(Long id) {
		BuckwaTest buckwaTest = em.find(BuckwaTest.class, id);
        return buckwaTest;
    }
	
    public BuckwaTest insert(BuckwaTest buckwaTest) {
        em.persist(buckwaTest);
        return buckwaTest;
    }
    
    public BuckwaTest update(BuckwaTest buckwaTest) {
        em.merge(buckwaTest);
        return buckwaTest;
    }
	
}
