package th.co.baiwa;

import static java.lang.System.exit;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(
	scanBasePackages = {
		"th.co.baiwa.buckwaframework",
		"th.co.baiwa.example"
	}
)
public class Application implements CommandLineRunner {
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println("Done!");
		exit(0);
	}
}
