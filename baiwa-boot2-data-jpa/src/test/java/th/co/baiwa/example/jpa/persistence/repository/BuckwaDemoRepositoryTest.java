package th.co.baiwa.example.jpa.persistence.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import th.co.baiwa.buckwaframework.common.config.AppConfigTest;
import th.co.baiwa.buckwaframework.common.constant.CommonConstants.PROFILE;
import th.co.baiwa.example.jpa.persistence.entity.BuckwaDemo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfigTest.class)
@ActiveProfiles(PROFILE.UNITTEST)
public class BuckwaDemoRepositoryTest {
	
	@Autowired
	private BuckwaDemoRepository buckwaDemoRepository;
	
	/*
	 * CRUD
	 */
//	@Test
	public void test_save_insert() {
		System.out.println("- - - - - test_save_insert");
		BuckwaDemo buckwaDemo = new BuckwaDemo();
		buckwaDemo.setDemoCode("001");
		buckwaDemo.setDemoDesc("Hello World");
		BuckwaDemo newBuckwaDemo = buckwaDemoRepository.save(buckwaDemo);
		System.out.println(ToStringBuilder.reflectionToString(newBuckwaDemo, ToStringStyle.JSON_STYLE));
	}
	
//	@Test
	public void test_saveAll() {
		System.out.println("- - - - - test_saveAll");
		
		List<BuckwaDemo> buckwaDemoList = new ArrayList<>();
		BuckwaDemo buckwaDemo = null;
		for (int i = 0; i < 10; i++) {
			buckwaDemo = new BuckwaDemo();
			buckwaDemo.setDemoCode(String.valueOf(1000 + (i + 1)));
			buckwaDemo.setDemoDesc("Desc" + String.valueOf(1000 + (i + 1)));
			buckwaDemoList.add(buckwaDemo);
		}
		
		buckwaDemoRepository.saveAll(buckwaDemoList);
	}
	
	@Test
	public void test_findById() {
		System.out.println("- - - - - test_findById");
		Optional<BuckwaDemo> opt = buckwaDemoRepository.findById(2L);
		Assert.assertTrue(opt.isPresent());
		opt.ifPresent(buckwaDemo -> {
			System.out.println(ToStringBuilder.reflectionToString(buckwaDemo, ToStringStyle.JSON_STYLE));
		});
	}
	
//	@Test
	public void test_existsById() {
		System.out.println("- - - - - test_existsById");
		boolean existFlag = buckwaDemoRepository.existsById(1L);
		Assert.assertTrue(existFlag);
		System.out.println("existFlag=" + existFlag);
	}
	
//	@Test
	public void test_findAll() {
		System.out.println("- - - - - test_findAll");
		List<BuckwaDemo> buckwaDemoList = buckwaDemoRepository.findAll();
		Assert.assertNotEquals(0, buckwaDemoList.size());
		buckwaDemoList.forEach(e -> System.out.println(ToStringBuilder.reflectionToString(e, ToStringStyle.JSON_STYLE)));
	}
	
//	@Test
	public void test_findAllById() {
		System.out.println("- - - - - test_findAllById");
		List<Long> ids = Arrays.asList(new Long[] {1L,2L,3L});
		List<BuckwaDemo> buckwaDemoList = (List<BuckwaDemo>) buckwaDemoRepository.findAllById(ids);
		Assert.assertNotEquals(0, buckwaDemoList.size());
		buckwaDemoList.forEach(e -> System.out.println(ToStringBuilder.reflectionToString(e, ToStringStyle.JSON_STYLE)));
	}
	
//	@Test
	public void test_count() {
		System.out.println("- - - - - test_count");
		long count = buckwaDemoRepository.count();
		Assert.assertNotEquals(0, count);
		System.out.println("count=" + count);
	}
	
//	@Test
	public void test_save_update() {
		System.out.println("- - - - - test_save_update");
		
		Optional<BuckwaDemo> opt = buckwaDemoRepository.findById(1L);
		opt.ifPresent(buckwaDemo -> {
			buckwaDemo.setDemoDesc("update na ja 3");
			buckwaDemoRepository.save(buckwaDemo);
		});
	}
	
//	@Test
	public void test_deleteById() {
		System.out.println("- - - - - test_deleteById");
		long demoId = buckwaDemoRepository.count();
		buckwaDemoRepository.deleteById(demoId);
	}
	
//	@Test
	public void test_delete() {
		System.out.println("- - - - - test_delete");
		long demoId = buckwaDemoRepository.count();
		Optional<BuckwaDemo> opt = buckwaDemoRepository.findById(demoId);
		opt.ifPresent(buckwaDemo -> {
			buckwaDemoRepository.delete(buckwaDemo);
		});
	}
	
//	@Test
	public void test_deleteByEntityList() {
		System.out.println("- - - - - test_deleteByEntityList");
		long demoId = buckwaDemoRepository.count();
		Optional<BuckwaDemo> opt = buckwaDemoRepository.findById(demoId);
		opt.ifPresent(e -> {
			List<BuckwaDemo> buckwaDemoList = new ArrayList<>();
			buckwaDemoList.add(e);
			buckwaDemoRepository.deleteAll(buckwaDemoList);
		});
	}
	
//	@Test
	public void test_deleteAll() {
		System.out.println("- - - - - test_deleteByEntityList");
		buckwaDemoRepository.deleteAll();
	}
	
	
	/*
	 * Query Methods
	 */
//	@Test
	public void test_findByDemoCode() {
		System.out.println("- - - - - test_findByDemoCode");
		BuckwaDemo buckwaDemo = buckwaDemoRepository.findByDemoCode("1001");
		System.out.println(buckwaDemo);
	}
	
//	@Test
	public void test_findByDemoDesc() {
		System.out.println("- - - - - test_findByDemoDesc");
		BuckwaDemo buckwaDemo = buckwaDemoRepository.findByDemoDesc("Desc1002");
		System.out.println(buckwaDemo);
	}
	
//	@Test
	public void test_findByDemoCodeAndDemoDesc() {
		System.out.println("- - - - - test_findByDemoCodeAndDemoDesc");
		BuckwaDemo buckwaDemo = buckwaDemoRepository.findByDemoCodeAndDemoDesc("1003", "Desc1003");
		System.out.println(buckwaDemo);
	}
	
//	@Test
	public void test_findAll_Sort() {
		System.out.println("- - - - - test_findAll_Sort");
		Sort sort = Sort.by(Direction.DESC, "demoCode");
		List<BuckwaDemo> buckwaDemoList = buckwaDemoRepository.findAll(sort);
		buckwaDemoList.forEach(e -> System.out.println(ToStringBuilder.reflectionToString(e, ToStringStyle.JSON_STYLE)));
	}
	
//	@Test
	public void test_findAll_Pageable() {
		System.out.println("- - - - - test_findAll_Pageable");
		//Pageable pageable = PageRequest.of(3, 10);
		Pageable pageable = PageRequest.of(3, 10, Direction.DESC, "demoCode");
		Page<BuckwaDemo> buckwaDemoList = buckwaDemoRepository.findAll(pageable);
		buckwaDemoList.forEach(e -> System.out.println(ToStringBuilder.reflectionToString(e, ToStringStyle.JSON_STYLE)));
	}
	
	
	/*
	 * JDBC
	 */
//	@Test
	public void test_findByCriteria() {
		System.out.println("- - - - - test_findByCriteria");
		
		BuckwaDemo buckwaDemo = new BuckwaDemo();
		buckwaDemo.setDemoDesc("100");
		
		List<BuckwaDemo> buckwaDemoList = buckwaDemoRepository.findByCriteria(buckwaDemo);
		buckwaDemoList.forEach(e -> System.out.println(ToStringBuilder.reflectionToString(e, ToStringStyle.JSON_STYLE)));
	}
	
//	@Test
	public void test_batchInsert() {
		System.out.println("- - - - - test_batchInsert");
		
		List<BuckwaDemo> buckwaDemoList = new ArrayList<>();
		BuckwaDemo buckwaDemo = null;
		for (int i = 0; i < 52; i++) {
			buckwaDemo = new BuckwaDemo();
			buckwaDemo.setDemoCode(String.valueOf(2000 + (i + 1)));
			buckwaDemo.setDemoDesc("Desc" + String.valueOf(2000 + (i + 1)));
			buckwaDemoList.add(buckwaDemo);
		}
		
		buckwaDemoRepository.batchInsert(buckwaDemoList, 20);
	}
	
}
