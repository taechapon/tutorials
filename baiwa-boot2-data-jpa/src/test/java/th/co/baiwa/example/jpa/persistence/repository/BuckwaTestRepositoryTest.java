 package th.co.baiwa.example.jpa.persistence.repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import th.co.baiwa.buckwaframework.common.config.AppConfigTest;
import th.co.baiwa.buckwaframework.common.constant.CommonConstants.PROFILE;
import th.co.baiwa.example.jpa.persistence.entity.BuckwaTest;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfigTest.class)
@ActiveProfiles(PROFILE.UNITTEST)
public class BuckwaTestRepositoryTest {
	
	@Autowired
	private BuckwaTestRepository buckwaTestRepository;
	
//	@Test
	public void test_save() {
		System.out.println("test_save");
		BuckwaTest buckwaTest = new BuckwaTest();
		buckwaTest.setColVarchar("Hello World");
		buckwaTest.setColInt(10);
		buckwaTest.setColDouble(20D);
		buckwaTest.setColDecimal(new BigDecimal("30"));
		buckwaTest.setColTimestamp(LocalDateTime.of(2018, Month.APRIL, 10, 16, 58, 03));
		buckwaTest.setColDate(LocalDate.of(2016, Month.JUNE, 5));
		buckwaTest.setColTime(LocalTime.of(10, 30));
		
		BuckwaTest newBuckwaTest = buckwaTestRepository.save(buckwaTest);
		System.out.println(ToStringBuilder.reflectionToString(newBuckwaTest, ToStringStyle.JSON_STYLE));
	}
	
//	@Test
	public void test_saveAll() {
		System.out.println("test_saveAll");
		
		BuckwaTest buckwaTest = null;
		List<BuckwaTest> buckwaTestList = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			buckwaTest = new BuckwaTest();
			buckwaTest.setColVarchar("Loop" + (i + 1));
			buckwaTest.setColInt(10 + (i + 1));
			buckwaTest.setColDouble(20D + (i + 1));
			buckwaTest.setColDecimal(new BigDecimal("30").add(new BigDecimal(i + 1)));
			buckwaTest.setColTimestamp(LocalDateTime.of(2018, Month.APRIL, 10 + (i + 1), 16, 58, 03));
			buckwaTest.setColDate(LocalDate.of(2016, Month.JUNE, 5 + (i + 1)));
			buckwaTest.setColTime(LocalTime.of(10, 30 + (i + 1)));
			buckwaTestList.add(buckwaTest);
		}
		
		List<BuckwaTest> newBuckwaTestList = (List<BuckwaTest>) buckwaTestRepository.saveAll(buckwaTestList);
		newBuckwaTestList.forEach(e -> System.out.println(ToStringBuilder.reflectionToString(e, ToStringStyle.JSON_STYLE)));
		
	}
	
	@Test
	public void test_findById() {
		System.out.println("test_findById");
		Optional<BuckwaTest> opt = buckwaTestRepository.findById(1L);
		opt.ifPresent(buckwaTest -> {
			System.out.println(ToStringBuilder.reflectionToString(buckwaTest, ToStringStyle.JSON_STYLE));
		});
	}

	@Test
	public void test_existsById() {
		System.out.println("test_exists");
		boolean existFlag = buckwaTestRepository.existsById(2L);
		System.out.println("existFlag=" + existFlag);
	}
	
	@Test
	public void test_findAll() {
		System.out.println("test_findAll");
		List<BuckwaTest> buckwaTestList = (List<BuckwaTest>) buckwaTestRepository.findAll();
		buckwaTestList.forEach(e -> System.out.println(ToStringBuilder.reflectionToString(e, ToStringStyle.JSON_STYLE)));
	}
	
	@Test
	public void test_count() {
		System.out.println("test_count");
		long count = buckwaTestRepository.count();
		System.out.println("count=" + count);
	}
	
	@Test
	public void test_deleteById() {
		System.out.println("test_deleteById");
		buckwaTestRepository.deleteById(buckwaTestRepository.count());
	}
	
	@Test
	public void test_findByColVarchar() {
		System.out.println("test_findByColVarchar");
		BuckwaTest buckwaTest = buckwaTestRepository.findByColVarchar("Loop1");
		System.out.println(ToStringBuilder.reflectionToString(buckwaTest, ToStringStyle.JSON_STYLE));
	}
	
	@Test
	public void test_findByColVarcharAndColInt() {
		System.out.println("test_findByColVarcharAndColInt");
		BuckwaTest buckwaTest = buckwaTestRepository.findByColVarcharAndColInt("Loop1", 11);
		System.out.println(ToStringBuilder.reflectionToString(buckwaTest, ToStringStyle.JSON_STYLE));
	}
	
	@Test
	public void test_findByPk() {
		System.out.println("test_findByPk");
		BuckwaTest buckwaTest = buckwaTestRepository.findByPk(4L);
		System.out.println(ToStringBuilder.reflectionToString(buckwaTest, ToStringStyle.JSON_STYLE));
	}
	
}
