package th.co.baiwa.example.jpa.service;

import java.sql.SQLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import th.co.baiwa.buckwaframework.common.config.AppConfigTest;
import th.co.baiwa.buckwaframework.common.constant.CommonConstants.PROFILE;
import th.co.baiwa.example.jpa.persistence.entity.BuckwaDemo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfigTest.class)
@ActiveProfiles(PROFILE.UNITTEST)
public class BuckwaDemoServiceTest {
	
	@Autowired
	private BuckwaDemoService buckwaDemoService;
	
	@Test(expected = SQLException.class)
	public void test_execute() throws SQLException {
		try {
			BuckwaDemo demo1 = new BuckwaDemo();
			demo1.setDemoCode("code001");
			demo1.setDemoDesc("desc001");
			
			BuckwaDemo demo2 = new BuckwaDemo();
			demo2.setDemoCode("code002");
			demo2.setDemoDesc("desc002");
			
			buckwaDemoService.execute(demo1, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SQLException(e.getMessage(), e);
		}
	}
	
}
