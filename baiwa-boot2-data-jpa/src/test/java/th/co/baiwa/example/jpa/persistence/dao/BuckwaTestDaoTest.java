package th.co.baiwa.example.jpa.persistence.dao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import th.co.baiwa.buckwaframework.common.config.AppConfigTest;
import th.co.baiwa.buckwaframework.common.constant.CommonConstants.PROFILE;
import th.co.baiwa.example.jpa.persistence.entity.BuckwaTest;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfigTest.class)
@ActiveProfiles(PROFILE.UNITTEST)
public class BuckwaTestDaoTest {
	
	@Autowired
	private BuckwaTestDao buckwaTestDao;
	
	@Test
	public void test_findById() {
		BuckwaTest buckwaTest = buckwaTestDao.findById(1L);
		System.out.println(ToStringBuilder.reflectionToString(buckwaTest, ToStringStyle.JSON_STYLE));
	}
	
//	@Test
	public void test_insert() {
		BuckwaTest buckwaTest = new BuckwaTest();
		buckwaTest.setColVarchar("Hello World EM");
		buckwaTest.setColInt(10);
		buckwaTest.setColDouble(20D);
		buckwaTest.setColDecimal(new BigDecimal("30"));
		buckwaTest.setColTimestamp(LocalDateTime.of(2018, Month.APRIL, 10, 16, 58, 03));
		buckwaTest.setColDate(LocalDate.of(2016, Month.JUNE, 5));
		buckwaTest.setColTime(LocalTime.of(10, 30));
		buckwaTestDao.insert(buckwaTest);
	}
	
//	@Test
	public void test_update() {
		BuckwaTest buckwaTest = buckwaTestDao.findById(18L);
		buckwaTest.setColVarchar("Edit here");
		buckwaTestDao.update(buckwaTest);
	}
	
//	@Test
	public void test_update_with_newObject() {
		BuckwaTest buckwaTest = new BuckwaTest();
		buckwaTest.setColPk(18L);
		buckwaTest.setColVarchar("Edit here new Object");
		buckwaTest.setColInt(20);
		buckwaTest.setColDouble(30D);
		buckwaTestDao.update(buckwaTest);
	}
	
//	@Test
	public void test_update_with_newObjectNoPk() {
		BuckwaTest buckwaTest = new BuckwaTest();
		buckwaTest.setColVarchar("Edit here new Object No PK");
		buckwaTest.setColInt(59);
		buckwaTest.setColDouble(69D);
		buckwaTestDao.update(buckwaTest);
	}
	
}
