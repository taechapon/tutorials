package th.co.baiwa.example.test.persistence.dao;

import static org.junit.Assert.assertNotEquals;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDaoTest {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Test
	public void testExecuteQueryWithBeanProperty() {
		String sql =
			" SELECT EMP_NO AS NO " +
			" 	,CONCAT(EMP_FNAME,EMP_LNAME) AS EMP_NAME " +
			" 	,EMP_EMAIL " +
			" 	,EMP_AGE " +
			" 	,EMP_SALARY " +
			" 	,EMP_HIRE_DATE " +
			" 	,CREATED_BY " +
			" 	,CREATED_DATE " +
			" FROM users ";
		
		List<Map<String, Object>> resultList = jdbcTemplate.query(sql, new ResultSetExtractor<List<Map<String, Object>>>() {
			@Override
			public List<Map<String, Object>> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Map<String, Object>> mapList = new ArrayList<>();
				Map<String, Object> map = null;
				
				int columnCount = rs.getMetaData().getColumnCount();
				String[] columnNames = new String[columnCount];
				for (int i = 0; i < columnCount; i++) {
					columnNames[i] = rs.getMetaData().getColumnLabel(i + 1);
					System.out.println("columnTypeName: " + rs.getMetaData().getColumnTypeName(i + 1));
					System.out.println("columnDisplaySize: " + rs.getMetaData().getColumnDisplaySize(i + 1));
					System.out.println("columnLabel: " + rs.getMetaData().getColumnLabel(i + 1));
					System.out.println("columnType: " + rs.getMetaData().getColumnType(i + 1));
					System.out.println("columnName: " + rs.getMetaData().getColumnName(i + 1));
					System.out.println("className: " + rs.getMetaData().getColumnClassName(i + 1));
					System.out.println(" - - - - - - - - - -");
				}
				
				while (rs.next()) {
					map = new HashMap<>();
					for (int i = 0; i < columnCount; i++) {
						try {
							map.put(columnNames[i], rs.getObject(i + 1, Class.forName(rs.getMetaData().getColumnClassName(i + 1))));
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						}
					}
					mapList.add(map);
				}
				
				return mapList;
			}
		});
		
		resultList.forEach(r -> r.forEach((k, v) -> System.out.println(k + " " + v)));
		
		assertNotEquals(0, resultList.size());
	}
	
}
